/* overwriting.c - exemplo de manipulação de Strings de fato
 * Autor: Cledson Ferreira
*/
/* cns_extras.h inclui "stdio.h" e "stdlib.h" */
#include <stdlib.h>
#include <cns_extras.h>
#include <locale.h>

int main() {
    cns_init();
    setlocale(LC_ALL, ""); // porque _count()
    cnstring unicorns = cnstring("Unicórnios tem chifres");

    /* Transformando essa frase em "O unicórnio tem chifre" */
    cns_remchar(unicorns, 0); // remove 'U'
    cns_putstr(unicorns, 0, "O u"); // adiciona "O u" no início

    /* Copiar seq. [2..13] da String 'unicorns':
     * "O <2>unicórnios<13> tem chifres"
     * Como acentos implicam em caracteres multibytes, então em vez de 12, o
     * comprimento real de "unicórnios" será 13 */
    cnstring cut = cns_cut(unicorns, 2, 13);
    cns_remstr(unicorns, cut, 0); // remover 'cut'
    cns_life_lose(cut);
    /* essas três linhas, neste caso, poderiam ser substituídas por:
     *     cns_remstr(unicorns, "unicórnios", 0);
     * esse 0 significa que deve ser removida a primeira palavra equivalente */

    cns_putstr(unicorns, 2, "unicórnio"); // adiciona unicórnio no índice 2
    cns_remchar(unicorns, cns_len(unicorns)-1); // remove última letra
    printf("String em 'unicorns'  = %s\n", cns_get(unicorns));
    printf("Tamanho de 'unicorns' = %lu\n", (unsigned long) cns_size(unicorns));
    printf("Comprimento da string = %lu\n", (unsigned long) cns_len(unicorns));
    printf("Número de caracteres  = %lu\n", (unsigned long) cns_count(unicorns));
    cns_life_lose(unicorns); // não precisamos mais de 'unicorns'
    return 0;
}
