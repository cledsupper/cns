/* cns_life.c - teste de vidas da cnstring.
 * O propósito deste código é mostrar uma rotina nova da libcnstring semelhante a
 * da biblioteca Glib.
 * Autor: Cledson Ferreira
 *
 * Compile e execute com os seguintes comandos:
 *  make clean
 *  make cnstring
 *  clang ex/cns_life.c lib/*.o -Iheaders -pthread -o bin/cns_life
 */

#include <cns_extras.h>

cnstring edit_cns(cnstring Str) {
    cns_life_get(Str);
    cns_println("\nedit_cns():\n"
                "escreva algo para a cnstring \"", Str, "\"");
    printf(" ... com %lu vidas restantes\n", Str->__life);
    cns_input(Str);
    cns_println("Devolvendo a show_cns() ou main()...");
    return cns_life_lose(Str); /* Retornaria NULL se não houvessem mais vidas */
}

void show_cns(cnstring Str) {
    /* cn_life é dependente dos compiladores GCC e Clang.
     * Em outros compiladores, apenas se altera para o tipo da variável */
    cn_life(cnstring) mStr = cns_life_get(Str);
    cns_println("\nshow_cns():\n"
                "estou com a cnstring \"", mStr, "\"");
    printf("Esta cnstring tem %lu vidas restantes\n", mStr->__life);
    cns_println("Direcionando-a para edit_cns()...");
    edit_cns(mStr);
    cns_println("\nshow_cns():\n"
                "edit_cns() alterou-a para \"", mStr, "\"");
    printf(" ... e agora ela tem %lu vidas.\n", mStr->__life);
    cns_println("Devolvendo a main()...");
    /* cns_life_lose(mStr); */
}

int main() {
    cn_life(cnstring) str1 = cnstring("str1");
    cns_println("Passando str1 para show_cns()...");
    show_cns(str1);
    cns_println("\nmain():\n"
                "Passando str1 para edit_cns()...");
    edit_cns(str1);
    printf("\nstr1 tem somente %lu vida(s) restante(s)\n", str1->__life);

    cns_println("Se nenhum aviso aparecer após isto, é porque a cnstring foi"
                " removida pela função de limpeza registrada com cn_auto");
    return 0;
}
