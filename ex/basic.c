//#!/usr/bin/tcc -run -I../headers/ ../src/cns*.c
/* basic.c - um exemplo de uso da biblioteca CNS em programas
 * Autor: Cledson Ferreira
 *
 * Compile e execute com os comandos:
 *  make clean
 *  make
 *
 * [TCC SOMENTE]
 * Você pode executar este programa diretamente se tiver o compilador TCC.
 * Descomente a primeira linha;
 * No terminal, neste diretório, faça:
 *  chmod +x ./basic.c # permite que o código seja executável
 *  ./basic.c # executa o programa
 */
#include <locale.h>
#include <stdlib.h>
#include <cns_extras.h>

int main() {
    char *meu_nome = "Ledso";
    setlocale(LC_ALL, "");
    // necessário definir 'locale' para usar _count() e
    // ... _towcs()
    printf("Eu sou %s, qual o seu nome?\n", meu_nome);
    cnstring seu_nome = cns_new();
    cns_input(seu_nome);
    int tries;
    for (tries = 0; cns_len(seu_nome) < 3 && tries < 2; tries++) {
        cns_println("Como?");
        cns_input(seu_nome);
    }
    wchar_t *wcs = cns_towcs(seu_nome);
    if (!wcs) {
        printf("Olá, %s!\n", cns_get(seu_nome));
        printf("Seu nome deve ter %lu letras :?\n", (unsigned long) cns_len(seu_nome));
    } else {
        printf("Oi, %ls!\n", wcs);
        printf("Seu nome tem %lu letras.\n", (unsigned long) cns_count(seu_nome));
        cn_free(wcs);
        /* A DIFERENÇA ENTRE cns_size(), cns_len() e cns_count()
         *
         * cns_size(): retorna o tamanho da string (em bytes);
         * cns_len(): retorna o tamanho da string (em bytes) - 1 (em teoria, sem contar o delimitador);
         * cns_count(): retorna o número de caracteres que a string contém. */
    }
    cns_life_lose(seu_nome);
    /* Veja cns_life.c */
    return 0;
}
