/* cns_search.c - um exemplo prático de como usar a função _search() da
 * biblioteca CNString.
 * Autor: Cledson Ferreira
 */
#include <cns_extras.h>

int main() {
    cns_init();
    cnstring teste1 = cnstring("teste teste vinte e quatro teste");
    cnstring entrada = cns_new();
    cnstring times = cns_new();

    printf("Insira uma palavra para pesquisar:\n");
    cns_input(entrada);

    long oc = cns_search(teste1, entrada, -1);
    cns_update(times, oc != 1 ? "vezes" : "vez");
    printf("\nA palavra \"%s\" foi encontrada %li %s na String.\n",
           cns_get(entrada), oc, cns_get(times));

    if (oc > 1 && oc < cns_size(teste1)) {
        printf("\nQual dessas vezes quer que eu indique?\n");
        scanf("%li", &oc);
    }
    if (oc > 0) {
        oc = cns_search(teste1, entrada, (oc > 0 ? oc-1 : 0));
        printf("\n");
        puts(cns_get(teste1));
        for (int i=0; i < oc; i++) {
            printf(".");
        }
        printf("^\n");
    }
    cns_life_lose(teste1);
    cns_life_lose(entrada);
    cns_life_lose(times);
    return 0;
}
