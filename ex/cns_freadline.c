// cns_freadline.c - Exemplo prático de como usar a função macro cns_freadline()
// by cleds'upper (Cledson Ferreira)

#include <cns_extras.h>
#include <stdlib.h>
#include <string.h>
// a ideia do CNS não é substituir as strings do C

#define QUIT_COMMAND "/quit"
#define YES_RESPONSE "sim"

bool leArquivo(cnstring endereco) {
    FILE *arq = fopen(cns_get(endereco), "r");
    if (!arq) return false;

    printf("\nLENDO ARQUIVO: ");
    cns_println(endereco);

    cnstring texto = newCNString();
    int i;
    for (i=1; cns_freadline(texto, arq); i++) {
        printf("%3d| ", i);
        cns_println(texto);
    }
    cns_life_lose(texto);
    fclose(arq);
    return true;
}

bool escreveArquivo(cnstring endereco) {
    FILE *arq = fopen(cns_get(endereco), "w");
    if (!arq) {
        fprintf(stderr, "\nERRO: erro ao criar o arquivo\n");
        return false;
    }

    printf("\nDigite \"%s\" em uma linha separada para sair e salvar\n", QUIT_COMMAND);
    printf("\nEDITANDO ARQUIVO: ");
    cns_println(endereco);

    cnstring texto = newCNString();
    int i=0;
    do {
        printf("%3d| ", i+1);
        cns_input(texto);
        if (!strcmp(cns_get(texto), QUIT_COMMAND)) break;
        if (i>0) fputc('\n', arq);
        cns_fwrite(texto, arq);
        i++;
    } while (!feof(arq));
    if (feof(arq)) {
        fprintf(stderr, "\nERRO ENQUANTO ESCREVIA NO ARQUIVO. ABORTADO!\n");
        exit(1);
    }
    printf("\nArquivo salvo!\n");
    cns_life_lose(texto);
    fclose(arq);
    return true;
}

int main() {
    cns_init();
    cnstring endereco = getCNString("teste.txt", FinalArg);
    cnstring resposta = newCNString();
    printf("Digite o endereço do arquivo:\n");
    cns_input(endereco);

    if (leArquivo(endereco)) {
        printf ("\nArquivo já existe, deseja sobrescrevê-lo? [sim/nao]\n");
        cns_setlower(cns_input(resposta));
        if (strcmp(cns_str(resposta), YES_RESPONSE)) {
            cns_life_lose(endereco);
            cns_life_lose(resposta);
            return 0;
        }
    }

    if (!escreveArquivo(endereco)) {
        cns_life_lose(endereco);
        cns_life_lose(resposta);
        return 1;
    }

    printf("\nReabrindo: %s ...\n", cns_str(endereco));
    if (!leArquivo(endereco)) {
        fprintf(stderr, "Erro ao tentar abrir arquivo!\n");
        cns_life_lose(endereco);
        cns_life_lose(resposta);
        exit(1);
    }

    printf("\nRemover arquivo? [sim/nao]\n");
    cns_input(resposta);
    if (!strcmp(cns_str(resposta), YES_RESPONSE))
        remove(cns_str(endereco));
    printf("\nby Ledso\n");
    cns_life_lose(endereco);
    cns_life_lose(resposta);
    return 0;
}
