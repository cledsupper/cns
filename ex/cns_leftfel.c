/* test.c - um exemplo de uso da biblioteca CNS em programas
 * Autor: Cledson Ferreira */
#include <locale.h>
#include <cns_extras.h>

int main() {
    cns_init();
    setlocale(LC_ALL, "");
    printf(" ---- LeftfeL by CNString versão %s alpha ----\n", cns_version_str_get());
    printf("Escreva uma palavra ou frase: ");
    cnstring entrada = cns_input(cnstring(""));
    cnstring adartne = cns_leftfel(entrada);
    printf("%s em leftfel:\n", cns_get(entrada));
    cns_println(adartne);

    cns_life_lose(entrada);
    cns_life_lose(adartne);
    return 0;
}
