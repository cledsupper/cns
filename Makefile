CC=clang
RUNOW=./
LIB_CONFIG= -I./headers/ -Wall -Werror
PKG_CONFIG= -I./headers/ lib/*.o -pthread

VER_REL=0
VER_SUB=11
VER_BUILD=510
LIBCNSTRING=libcnstring.so
LIBCNSTRING_STABLE=libcnstring.so.$(VER_REL)
LIBCNSTRING_UNSTAB=libcnstring.so.$(VER_REL).$(VER_SUB)
LIBCNSTRING_RSBVER=libcnstring.so.$(VER_REL).$(VER_SUB).$(VER_BUILD)

all: cnstring example-basic

clean:
	rm -f lib/*.o lib/*.so* bin/*

example-basic:
	mkdir -p bin
	$(CC) $(PKG_CONFIG) ex/basic.c -o bin/basic
	cd bin && $(RUNOW)basic

links:
	ln -s $(LIBCNSTRING) lib/libcnstring.so
	ln -s $(LIBCNSTRING) lib/$(LIBCNSTRING_STABLE)
	ln -s $(LIBCNSTRING) lib/$(LIBCNSTRING_UNSTAB)
	ln -s $(LIBCNSTRING) lib/$(LIBCNSTRING_RSBVER)

shared: cnstring-shared
	$(CC) -shared  $(PKG_CONFIG) -o lib/$(LIBCNSTRING)

cnstring:
	mkdir -p lib
	$(CC) -c $(LIB_CONFIG) src/cnstring.c    -o lib/cnstring.o
	$(CC) -c $(LIB_CONFIG) src/cns_extras.c  -o lib/cns_extras.o
	$(CC) -c $(LIB_CONFIG) src/cns_leftfel.c -o lib/cns_leftfel.o
	$(CC) -c $(LIB_CONFIG) src/cns_cut.c     -o lib/cns_cut.o
	$(CC) -c $(LIB_CONFIG) src/cns_search.c  -o lib/cns_search.o
	$(CC) -c $(LIB_CONFIG) src/cns_arrays.c  -o lib/cns_arrays.o
	$(CC) -c $(LIB_CONFIG) src/cns_string.c  -o lib/cns_string.o

cnstring-shared:
	$(CC) -fPIC -c $(LIB_CONFIG) src/cnstring.c    -o lib/cnstring.o
	$(CC) -fPIC -c $(LIB_CONFIG) src/cns_extras.c  -o lib/cns_extras.o
	$(CC) -fPIC -c $(LIB_CONFIG) src/cns_leftfel.c -o lib/cns_leftfel.o
	$(CC) -fPIC -c $(LIB_CONFIG) src/cns_cut.c     -o lib/cns_cut.o
	$(CC) -fPIC -c $(LIB_CONFIG) src/cns_search.c  -o lib/cns_search.o
	$(CC) -fPIC -c $(LIB_CONFIG) src/cns_arrays.c  -o lib/cns_arrays.o
	$(CC) -fPIC -c $(LIB_CONFIG) src/cns_string.c  -o lib/cns_string.o
