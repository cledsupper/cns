/* cns_extras.h - manipulação da CNString com outros tipos de objetos.
 *
 * Copyright (C) 2018  Cledson Ferreira
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#define CNS_COMPILING 1
#include <cns_extras.h>

#ifdef __cplusplus
#include <cstdlib>
#else
#include <stdlib.h>
#endif /* __cplusplus */

#define _CNSIZE_INCREMENT 255

cnstring cns_freadline(cnstring Str, FILE *_instream) {
    _CNS_AVOID_NULL_POINTER(Str, _freadline(), 1, NULL);
    _CNS_AVOID_NULL_POINTER(_instream, _freadline(), 2, NULL);
    if (feof(_instream))
        return NULL;

    int c = fgetc(_instream);
    char *str = CSTRING(Str->string_c);
    size_t i, len = Str->size-1;
    for (i=0; c!=EOF; i++) {
        if (c == '\n') break;
        if (i >= len) {
            if (!cns_resize(Str, Str->size+_CNSIZE_INCREMENT)) {
                _CNS_WARNING(_freadline());
                return NULL;
            }
            /* Pessoas dizem que operar C-string como ponteiro em vez de array
             * otimiza o programa */
            str = CSTRING(Str->string_c)+i;
            len = Str->size-1;
        }
        *str++ = (char)c;
        c = fgetc(_instream);
    }
    return cns_resize(Str, i+1);
}

cnstring cns_fread(cnstring Str, FILE *_instream) {
    _CNS_AVOID_NULL_POINTER(Str, _freadline(), 1, NULL);
    _CNS_AVOID_NULL_POINTER(_instream, _freadline(), 2, NULL);
    if (feof(_instream))
        return NULL;
    long len = 0;
    { /* Falta informação na internet sobre SEEK_END, mas por mim, dane-se!
       * vou usá-lo como se ele estivesse em todas as implementações do C */
        int curr_pos = ftell(_instream);
        fseek(_instream, 0, SEEK_END);
        len = ftell(_instream)-curr_pos;
        fseek(_instream, curr_pos, SEEK_SET);
    }
    if (len < 0) return NULL;
    if (!len) return Str;
    else if (!cns_resize(Str, (size_t)len+1)) {
        _CNS_WARNING(_fread());
        return NULL;
    }
    if (fread(CSTRING(Str->string_c), sizeof(char), (size_t)len, _instream)!=(size_t)len)
        return NULL;
    return Str;
}
#undef _CNSIZE_INCREMENT

FILE *cns_fwrite(cnstring Str, FILE *_outstream) {
    _CNS_AVOID_NULL_POINTER(Str, _fwrite(), 1, NULL);
    _CNS_AVOID_NULL_POINTER(_outstream, _fwrite(), 2, NULL);
    return fputs(Str->string_c, _outstream)>=0 ? _outstream : NULL;
}