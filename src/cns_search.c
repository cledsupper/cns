/* cns_search.h - pesquisa em C(strings)
 *
 * Copyright (C) 2018  Cledson Ferreira
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#define CNS_COMPILING 1
#include <cns/cns_search.h>

#ifndef __cplusplus
#include <string.h>
#else
#include <cstring>
#endif

long cns_search(const void *_string, const void *str_seq, int skip) {
    if (str_seq == _string)
        return 0;
    _CNS_AVOID_NULL_POINTER(_string, _search(), 1, (skip>=0 ? -1 : 0)); 
    _CNS_AVOID_NULL_POINTER(str_seq, _search(), 2, (skip>=0 ? -1 : 0));

    size_t size;
    const char *str_c;
    if (cns_is(_string)) {
        str_c = CNSTRING(_string)->string_c;
        size = CNSTRING(_string)->size;
    } else {
        str_c = CSTRING(_string);
        size = strlen(str_c)+1;
    }
    size_t seqsize;
    const char *seqstr;
    if (cns_is(str_seq)) {
        seqstr = CNSTRING(str_seq)->string_c;
        seqsize = CNSTRING(str_seq)->size;
    } else {
        seqstr = CSTRING(str_seq);
        seqsize = strlen(seqstr)+1;
    }

    if (!seqstr[0])          return (skip>=0 ? skip : (long)size);
    if (seqsize > size) return (skip>=0 ? -1 : 0);
    if (!str_c[0])   return (skip>=0 ? -1 : 0);

    long s = -1;
    size_t x;
    for (x=0; x < size-1; x++) {
        if (str_c[x] == seqstr[0]) {
            size_t y;
            for (y=x; y-x < seqsize; y++) {
                if (y-x == seqsize-1) {
                    if (++s < skip || skip < 0)
                        break;
                    return (int)x;
                }
                if (y >= size)
                    return -1;
                if (str_c[y] != seqstr[y-x])
                    break;
            }
        }
    }
    if (skip < 0) return s+1;
    return -1;
}
