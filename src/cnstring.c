/* cnstring.c - biblioteca para cnstrings em C/C++
 *
 * Copyright (C) 2018  Cledson Ferreira
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#define CNS_COMPILING 1
#include <cns_extras.h>

#ifndef __cplusplus
#include <assert.h>
#include <string.h>
#include <stdarg.h>
#include <wctype.h>
#else
#include <cassert>
#include <cstring>
#include <cstdarg>
#include <cwctype>
#endif

static struct _cns {
    size_t        max_size;
    long unsigned meta;
#ifndef CNS_DISABLE_FEATURES
    bool          up;
    struct _static_block {
        cnmutex  lock;
        CNString *last;
    } _static;
#endif
} cns = {_CNS_DEFAULT_MAX_SIZE, 0
#ifndef CNS_DISABLE_FEATURES
         , false
#endif
};

size_t _cns_max_size_set(size_t max_size) {
    if (max_size>1)
        cns.max_size = max_size;
    return cns.max_size;
}

bool cns_version_check(int ver_release, int ver_sub, int ver_build) {
    long subver = ver_sub*10000 + ver_build;
    long lib_subver = CNS_VERSION_SUB*10000 + CNS_VERSION_BUILD;
    if (ver_release < CNS_VERSION_RELEASE) return true;
    if (ver_release == CNS_VERSION_RELEASE)
        return subver <= lib_subver;
    return false;
}

const char *cns_version_str_get() {
    return CNS_VERSION_STR;
}

bool cns_is(const void *maybeCNString) {
    _CNS_AVOID_NULL_POINTER(maybeCNString, _is(), 1, false);
    if (!cns.meta) return false;
    return CNSTRING(maybeCNString)->__cns_meta == cns.meta;
}

#ifndef CNS_DISABLE_FEATURES
void _cns_assert(const char *expr, int src_line, const char *src_func, const char *src_fname) {
    int i = 0;
    for (; src_fname[i]; i++);
    bool one_separator = false;
    for (i--; i>0; i--) {
        if (src_fname[i]=='/' || src_fname[i]=='\\') {
            if (!one_separator) one_separator=true;
            else {
                src_fname+=i+1;
                break;
            }
        }
    }
    fprintf(stderr, " *** libCNString by _assert at %s, line %d of \"%s\":"
            "\n ... failed to assert that `%s`!\n", src_func, src_line, src_fname,
            expr);
    cns_abort();
}
#endif

void _cns_print(const void *_string, ...) {
    const void *arg = _string;
    va_list args;
    va_start(args, _string);
    while (arg != FinalArg) {
        if (cns_is(arg))
            fputs(CNSTRING(arg)->string_c, stdout);
        else fputs(CSTRING(arg), stdout);
        arg = va_arg(args, void*);
    }
    va_end(args);
}

void _cns_printerr(const void *_string, ...) {
    const void *arg = _string;
    va_list args;
    va_start(args, _string);
    while (arg != FinalArg) {
        if (cns_is(arg))
            fputs(CNSTRING(arg)->string_c, stderr);
        else fputs(CSTRING(arg), stderr);
        arg = va_arg(args, void*);
    }
    va_end(args);
}

#define _CNS_ERROR_REALLOC(__FUNCTION)\
    fprintf(stderr, "\n *** libCNString at " #__FUNCTION \
    ", line %d of \"%s\", says:  failed to resize the cnstring!\n", __LINE__,\
    __FILE__)

#define _CNS_ERROR_ALLOC_CSTRING(__FUNCTION)\
    fprintf(stderr, "\n *** libCNString at " #__FUNCTION \
    ", line %d of \"%s\", says:"\
        " failed to create a char array!\n", __LINE__, __FILE__)

/** Aloca memória para uma C-string e concatena com '_str_first' e os
 * parâmetros de 'args'.
 * 
 * Talvez deixe de ser uma função estática se outros programadores assim
 * desejarem. Afinal, pode ser bastante útil com a função _remount().
 * 
 * Um parâmetro nulo ou uma string de formato vai encerrar o loop.
 * Sempre retornará uma C-string válida, a menos que não consiga criar uma string
 * de início.
 *
 * @param _str_first - uma C-string ou CNString diferente de NULL.
 * @param args - todos os parâmetros dessa va_list devem ser C-strings ou
 * CNstrings.
 * @return char*, NULL - erro */
static char *_cns_init_string(const void *_str_first, va_list args, size_t *size) {
    *size = 1;
    char *string_c = cn_new(char, *size);
    if (!string_c) {
        _CNS_ERROR_ALLOC_CSTRING(_cns_init_string());
        return NULL;
    }
    const void *_string = _str_first;
    size_t c=0; // posição corrente do buffer string_c
    while (_string!=FinalArg) {
        char *re_string_c;
        if (cns_is(_string)) {
            *size += CNSTRING(_string)->size-1;
            if (*size > cns.max_size) {
                _CNS_MAXSIZE_OVERFLOW_WARNING(_cns_init_string());
                string_c[c] = '\0';
                *size -= CNSTRING(_string)->size-1;
                return string_c;
            }
            re_string_c = cn_resize(string_c, char, *size);
            if (!re_string_c) {
                _CNS_ERROR_REALLOC(_cns_init_string());
                string_c[c] = '\0';
                return string_c;
            }
            string_c = re_string_c;
            strncpy(string_c+c, CNSTRING(_string)->string_c,
                    CNSTRING(_string)->size-1);
            c += CNSTRING(_string)->size-1;
        } else {
            size_t i = strlen(CSTRING(_string));
            if (*size+i > cns.max_size) {
                _CNS_MAXSIZE_OVERFLOW_WARNING(_cns_init_string());
                string_c[c] = '\0';
                return string_c;
            }
            *size += i;
            re_string_c = cn_resize(string_c, char, *size);
            if (!re_string_c) {
                _CNS_ERROR_REALLOC(_cns_init_string());
                string_c[c] = '\0';
                *size -= i;
                return string_c;
            }
            string_c = re_string_c;
            strncpy(string_c+c, CSTRING(_string), i);
            c += i;
        }
        _string = va_arg(args, void*);
    }
    string_c[c] = '\0';
    return string_c;
}

#ifndef CNS_DISABLE_FEATURES
#define _CNS_LOCK_FAIL(__FUNCTION) {\
        fprintf(stderr, " *** libCNString says: failed to acquire exclusive access!\n");\
        fprintf(stderr, " ... at " #__FUNCTION ", line %d of \"%s\"\n", __LINE__, __FILE__);\
}

static bool _cns_static_add(CNString *Str) {
    if (!cnmutex_timedlock(&(cns._static.lock), 30)) {
        _CNS_LOCK_FAIL(_static_add());
        return false;
    }
    if (cns._static.last)
        (cns._static.last)->__next = Str;
    Str->__prev = cns._static.last;
    Str->__next = NULL;
    cns._static.last = Str;
    cnmutex_unlock(&(cns._static.lock));
    return true;
}

static bool _cns_static_remove(CNString *Str) {
    if (!cnmutex_timedlock(&(cns._static.lock), 30)) {
        _CNS_LOCK_FAIL(_static_remove());
        return false;
    }
    if (Str->__prev) (Str->__prev)->__next = Str->__next;
    if (Str == cns._static.last)
        cns._static.last = Str->__prev;
    else if (Str->__next) (Str->__next)->__prev = Str->__prev;
    cnmutex_unlock(&(cns._static.lock));
    return true;
}
#endif /* !CNS_DISABLE_FEATURES */

#define _CNS_ERROR_ALLOC(__FUNCTION)\
    fprintf(stderr, "\n *** libCNString at " #__FUNCTION \
    ", line %d of \"%s\", says: failed to create a String!\n", __LINE__,\
    __FILE__)

CNString* newCNString() {
#ifndef CNS_DISABLE_FEATURES
    assert(cns.up);
#endif
    CNString* Str = cn_new(CNString, 1);
    if (!Str) {
        _CNS_ERROR_ALLOC(newCNString());
        return Str;
    }
    Str->__life = 1;
    Str->string_c = cn_new(char, 1);
    if (!Str->string_c) {
        _CNS_ERROR_ALLOC_CSTRING(newCNString());
        cn_free(Str);
        return NULL;
    }
    Str->size = 1;
    if (!cns.meta)
        cns.meta = (unsigned long) Str;
    Str->__cns_meta = cns.meta;
#ifndef CNS_DISABLE_FEATURES
    if (!_cns_static_add(Str)) {
        _CNS_WARNING(newCNString());
        cn_free(Str->string_c);
        cn_free(Str);
        return NULL;
    }
#endif
    return Str;
}

CNString* getCNString(const void *_string, ...) {
    CNString* Str = NULL;
    if (_string != FinalArg) {
        va_list args;
        va_start(args, _string);
        size_t size;
        char* string_c = _cns_init_string(_string, args, &size);
        va_end(args);
        if (!string_c) {
            _CNS_WARNING(getCNString());
            return Str;
        }
        Str = newCNString();
        if (!Str) {
            _CNS_WARNING(getCNString());
            cn_free(string_c);
            return Str;
        }
        cn_free(Str->string_c);
        Str->string_c = string_c;
        Str->size = size;
    } else if (!(Str = newCNString())) {
        _CNS_WARNING(getCNString());
        return Str;
    }
    return Str;
}

CNString* _cns_update(CNString* Str, const void *_string, ...) {
    char *string_c = Str->string_c;
    size_t size = Str->size;

    if (_string != FinalArg) {
        va_list args;
        va_start(args, _string);
        Str->string_c = _cns_init_string(_string, args, &Str->size);
        va_end(args);
        if (!Str->string_c) {
            _CNS_WARNING(_update());
            Str->string_c = string_c;
            Str->size = size;
            return NULL;
        }
        cn_free(string_c);
    } else {
        for (size=0; size < Str->size; size++)
            if (Str->string_c[size] == 0)
                break;
        if (size >= cns.max_size) {
            Str->string_c[0] = '\0';
            size = 0; /* será incrementado abaixo :P */
        }
        Str->string_c = cn_resize(Str->string_c, char, size+1);
        Str->size = size+1;
    }
    return Str;
}

CNString* cns_format(CNString* Str, const char *format, ...) {
    va_list args;
    char *old = Str->string_c;
    va_start(args, format);
    int len;
#if GCC_VERSION && !defined(CNS_DISABLE_FEATURES)
    len = vasprintf(&Str->string_c, format, args);
    if (len <= 0) {
        _CNS_WARNING(_format());
	if (Str->string_c && Str->string_c != old)
            cn_free(Str->string_c);
	Str->string_c = old;
    }
#else
    Str->string_c = cn_new(char, Str->size);
    if (!Str->string_c) {
        _CNS_ERROR_ALLOC(_format());
	Str->string_c = old;
	return NULL;
    }
    len = vsnprintf(Str->string_c, Str->size, format, args);
    if (len <= 0) {
        _CNS_WARNING(_format());
	cn_free(Str->string_c);
	Str->string_c = old;
	return NULL;
    }
#endif
    cn_free(old);
    va_end(args);
    return cns_resize(Str, len+1);
}

CNString* cns_resize(CNString* Str, size_t size) {
    bool Str_allocd_here = false;
    if (!Str && size) {
        Str = newCNString();
        if (!Str) {
            _CNS_WARNING(_resize());
	    return Str;
        } Str_allocd_here = true;
    }
    if (!size) {
        if (Str) {
            cns_delete(Str);
            return NULL;
        }
	else return Str;
    }
    else if (size == Str->size)
        return Str;
    else if (size > cns.max_size) {
        if (Str_allocd_here) cns_delete(Str);
        _CNS_MAXSIZE_OVERFLOW_WARNING(_resize(size > max_size));
        return NULL;
    }

    char *string_c = Str->string_c;
    Str->string_c = cn_resize(string_c, char, size);
    if (!Str->string_c) {
        _CNS_ERROR_REALLOC(_resize());
        Str->string_c = string_c;
        if (Str_allocd_here) cns_delete(Str);
        return NULL;
    }
    Str->size=size;
    Str->string_c[size-1]='\0';
    return Str;
}

#ifndef CNS_DISABLE_FEATURES
#define _CNS_ERROR_MOUNT()\
    fprintf(stderr, "\n *** libCNString says: failed to mount the string!")

CNString* _cns_remount(CNString* Str, char *str) {
    if (Str->string_c == str)
        return _cns_update(Str, FinalArg);

    size_t i = strlen(str);
    if (i >= cns.max_size) {
        _CNS_MAXSIZE_OVERFLOW_WARNING(_remount());
        return NULL;
    }

    char *old = Str->string_c;
    Str->string_c = cn_resize(str, char, i+1);
    if (!Str->string_c) {
        _CNS_ERROR_REALLOC(_remount()); _CNS_ERROR_MOUNT();
        Str->string_c = old;
        return NULL;
    }
    Str->size = i+1;
    if (old) cn_free(old);
    return Str;
}
#endif /* !CNS_DISABLE_FEATURES */


CNString* cns_copy(CNString* dest, CNString* src) {
    if (dest == src) return dest;

    if (!cns_resize(dest, src->size)) {
        _CNS_WARNING(_copy());
        return NULL;
    }
    strcpy(dest->string_c, src->string_c);
    return dest;
}

CNString* cns_append(CNString* dest, CNString* src) {
    size_t dest_end = dest->size-1;
    size_t src_size = src->size;
    if(!cns_resize(dest, dest_end+src_size)) {
        _CNS_WARNING(_append());
        return NULL;
    }
    strcpy(dest->string_c+dest_end, src->string_c);
    return dest;
}

CNString* cns_clone(CNString* Str) {
    CNString* clone = NULL;
    if (!(clone = cns_resize(clone, Str->size))) {
        _CNS_WARNING(_clone());
        return NULL;
    }
    strcpy(clone->string_c, Str->string_c);
    return clone;
}


bool cns_equals(const void *str1, const void *str2) {
    if (str1 == str2) return true;
    if (!str1 || !str2) return false;
    const char *str_c[2];
    size_t size[2];
    if (cns_is(str1)) {
        str_c[0] = CNSTRING(str1)->string_c;
        size[0] = CNSTRING(str1)->size;
    } else {
        str_c[0] = CSTRING(str1);
        size[0] = strlen(str_c[0])+1;
    }
    if (cns_is(str2)) {
        str_c[1] = CNSTRING(str2)->string_c;
        size[1] = CNSTRING(str2)->size;
    } else {
        str_c[1] = CSTRING(str2);
        size[1] = strlen(str_c[1])+1;
    }
    if (size[0] != size[1]) return false;
    return !strncmp(str_c[0], str_c[1], size[0]);
}

CNString* cns_setchar(CNString* Str, size_t index, char c) {
    if (index < Str->size && !c)
        return cns_resize(Str, index+1);
    else if (index < Str->size-1) {
        Str->string_c[index] = c;
    } else {
        _CNS_ERROR_INVALID_INDEX(_setchar(), index, Str->size-1);
        return NULL;
    }
    return Str;
}

CNString* cns_putchar(CNString* Str, size_t index, char c) {
    if (index < Str->size) {
        if (!c)
            return cns_resize(Str, index+1);
        else if (cns_resize(Str, Str->size+1)) {
            size_t i;
            for (i=Str->size-2; i > index; i--)
                Str->string_c[i] = Str->string_c[i-1];
            Str->string_c[i] = c;
        } else {
            _CNS_WARNING(_putchar());
            return NULL;
        }
    } else {
        _CNS_ERROR_INVALID_INDEX(_putchar(), index, Str->size);
        return NULL;
    }
    return Str;
}

CNString* cns_remchar(CNString* Str, size_t index) {
    if (index < Str->size-1) {
        size_t i;
        for (i=index; i < Str->size-1; i++)
            Str->string_c[i] = Str->string_c[i+1];
        return cns_resize(Str, Str->size-1);
    }
    _CNS_ERROR_INVALID_INDEX(_remchar(), index, Str->size-1);
    return NULL;
}


#define _CNS_ERROR_DELETING_NONCNS(__FUNCTION, __ARG_V, __RET_V)\
    if (!cns_is(__ARG_V)) {\
        fprintf(stderr, "\n *** libCNString says: can NOT delete the non-"\
            "CNString pointer %p!\n", (void*)__ARG_V);\
        _CNS_WARNING(__FUNCTION);\
        return __RET_V;\
    }

bool cns_delete(CNString* Str) {
    _CNS_ERROR_DELETING_NONCNS(_delete(), Str, false);
#ifndef CNS_DISABLE_FEATURES
    if (!_cns_static_remove(Str)) {
        _CNS_WARNING(_delete());
        return false;
    }
#endif
    cn_free(Str->string_c);
    cn_free(Str);
    return true;
}

bool cns_zdelete(CNString* StrPass) {
    _CNS_ERROR_DELETING_NONCNS(_zdelete(), StrPass, false);
    memset(StrPass->string_c, 0, StrPass->size-1);
#ifndef CNS_DISABLE_FEATURES
    if (!_cns_static_remove(StrPass)) {
        _CNS_WARNING(_delete());
        return false;
    }
#endif
    cn_free(StrPass->string_c);
    cn_free(StrPass);
    return true;
}

cnstring cns_life_get(cnstring Str) {
    Str->__life++;
    return Str;
}

cnstring cns_life_lose(cnstring Str) {
    _CNS_ERROR_DELETING_NONCNS(_life_lose(), Str, NULL);
    if (!(--Str->__life)) {
        if (_cns_static_remove(Str)) {
            cn_free(Str->string_c);
	    cn_free(Str);
            Str = NULL;
        } else {
            _CNS_WARNING(_life_lose());
            Str->__life++;
        }
    }
    return Str;
}

#ifndef CNS_DISABLE_FEATURES
void cns_kill() {
    if (!cns.up) return;
    unsigned long counts=0;
    CNString *previous;
    if (cns._static.last) {
        if (!cnmutex_timedlock(&(cns._static.lock), 30)) {
            _CNS_LOCK_FAIL(_kill());
            abort();
        }
        do {
            previous = cns._static.last->__prev;
            if ((cns._static.last)->string_c)
                cn_free((cns._static.last)->string_c);
            cn_free(cns._static.last);
            counts++;
            cns._static.last = previous;
        } while (previous);
	cnmutex_unlock(&(cns._static.lock));
#if !defined(CNS_SUPPRESS_WARNINGS) && !defined(NDEBUG)
        printf("\n * libCNString says: freed %lu*2 static blocks\n", counts);
#endif
    }
    cnmutex_destroy(&(cns._static.lock));
    cns.up = false;
}

void _cns_init() {
    if (!cns.up) {
        cnmutex_init(&(cns._static.lock));
	cns._static.last = NULL;
        cns.up = true;
    }
}
#endif /* !CNS_DISABLE_FEATURES */
