/* cns_string.c - funções para usos gerais de (C/cn)(w)strings
 *
 * Copyright (C) 2018  Cledson Ferreira
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#define CNS_COMPILING 1
#include <cns_extras.h>

#ifndef __cplusplus
#include <string.h>
#include <wctype.h>
#else
#include <cstring>
#include <cwctype>
#endif

#if !defined(CNS_SUPPRESS_WARNINGS) && !defined(NDEBUG)
#define _CNS_INVALID_LOCALE_SET_WARNING(__FUNCTION)\
    fprintf(stderr," * libCNString at " #__FUNCTION ", "\
            "line %d of \"%s\", says: unknown string encoding\n"\
            " ... length defaults to 0\n", __LINE__, __FILE__);
#define _CNS_INVALID_MBCHAR_WARNING(__FUNCTION)\
    fprintf(stderr, " * libCNString at " #__FUNCTION ", line %d of \"%s\", says:"\
            "got an invalid/incomplete char, but ignoring it...\n", __LINE__, __FILE__);
#else
#define _CNS_INVALID_LOCALE_SET_WARNING(__FUNCTION)
#define _CNS_INVALID_MBCHAR_WARNING(__FUNCTION)
#endif

size_t cns_count(const void *_string) {
    const char* str_c;
    size_t size;
    if (cns_is(_string)) {
        str_c = CNSTRING(_string)->string_c;
        size = CNSTRING(_string)->size;
    } else {
        str_c = CSTRING(_string);
        size = strlen(str_c)+1;
    }

    mbstate_t mbs;
    memset(&mbs, 0, sizeof(mbs));
    size_t length = 0;
    for (; size > length; length++) {
        size_t wclen = mbrlen(str_c, MB_CUR_MAX, &mbs);
        if (!wclen) break;
	else if (wclen == (size_t)-1) {
            _CNS_INVALID_MBCHAR_WARNING(_leftfel());
	    wclen = 1;
	} else if (wclen == (size_t)-2) {
            _CNS_INVALID_LOCALE_SET_WARNING(_leftfel());
	    length = 0;
	    break;
	}
        str_c += wclen;
    }
    return length;
}

CNString* cns_setstr(CNString* Str, size_t index, const void* _string) {
    if (index >= Str->size-1) {
        _CNS_ERROR_INVALID_INDEX(_setstr(), index, Str->size-1);
        return NULL;
    }
    /* 'i' apenas incialmente é o comprimento de seqstr */
    size_t i;
    const char* strseq;
    if (cns_is(_string)) {
        strseq = CNSTRING(_string)->string_c;
        i = CNSTRING(_string)->size-1;
    } else {
        strseq = CSTRING(_string);
        i = strlen(strseq);
    }
    size_t size = index+i+1;
    if (size >= Str->size) {
        if (!cns_resize(Str, size)) {
            _CNS_WARNING(_setstr());
            return NULL;
        }
    }
    for (i=index; i < Str->size-1; i++)
        Str->string_c[i] = strseq[i-index];
    return Str;
}

CNString* cns_putstr(CNString* Str, size_t index, const void* _string) {
    if (index >= Str->size) {
        _CNS_ERROR_INVALID_INDEX(_putstr(), index, Str->size-1);
        return NULL;
    }
    size_t str_len;
    const char *str_seq;
    if (cns_is(_string)) {
        str_seq = CNSTRING(_string)->string_c;
        str_len = CNSTRING(_string)->size-1;
    } else {
        str_seq = CSTRING(_string);
        str_len = strlen(str_seq);
    }
    size_t i;
    /* TODO: este código deve ser otimizado! */
    for (i=0; i < str_len; i++) {
        if (!cns_putchar(Str, index+i, str_seq[i])) {
            _CNS_WARNING(_putstr());
            return NULL;
        }
    }
    return Str;
}

CNString* cns_remstr(CNString* Str, const void* _string, unsigned int skip) {
    long at = cns_search(Str, _string, (int)skip);
    size_t seqsize;
    if (at < 0)
        return NULL;
    if (cns_is(_string))
        seqsize = CNSTRING(_string)->size;
    else seqsize = strlen(CSTRING(_string))+1;
    size_t i;
    for (i=(size_t)at; (i-(size_t)at) < seqsize-1; i++) {
        cns_remchar(Str, (size_t)at);
    }
    return Str;
}

void *cns_setUPPER(void *_string) {
    size_t size;
    char* string_c;
    if (cns_is(_string)) {
        string_c = CNSTRING(_string)->string_c;
        size = CNSTRING(_string)->size;
    } else {
        string_c = CSTRING(_string);
        size = strlen(string_c)+1;
    }
    wchar_t *wstr = cns_towcs(_string);
    if (!wstr) {
        _CNS_WARNING(_setUPPER());
        return _string;
    }
    size_t i;
    for (i = 0; i < size-1; i++)
        if (iswlower(wstr[i]))
            wstr[i] = towupper(wstr[i]);
    wcstombs(string_c, wstr, size);
    cn_free(wstr);
    return _string;
}

void* cns_setlower(void *_string) {    
    size_t size;
    char* string_c;
    if (cns_is(_string)) {
        string_c = CNSTRING(_string)->string_c;
        size = CNSTRING(_string)->size;
    } else {
        string_c = CSTRING(_string);
        size = strlen(string_c)+1;
    }
    wchar_t *wstr = cns_towcs(_string);
    if (!wstr) {
        _CNS_WARNING(_setUPPER());
        return _string;
    }
    size_t i;
    for (i = 0; i < size-1; i++)
        if (iswupper(wstr[i]))
            wstr[i] = towlower(wstr[i]);
    wcstombs(string_c, wstr, size);
    cn_free(wstr);
    return _string;
}

#ifdef __cplusplus
std::string cns_tostring(std::string& string_c, CNString *Str) {
    string_c = std::string(cns_get(Str));
    return string_c;
}

char* cns_tostringc(char *string_c, CNString *Str) {
    size_t i;
    for (i=0; i < cns_size(Str); i++)
        string_c[i] = Str->string_c[i];
    return string_c;
}
#else
/* é necessário que 'string_c' suporte 'Str' */
char* cns_tostring(char *string_c, CNString *Str) {
    size_t i;
    for(i=0; i < cns_size(Str); i++)
        string_c[i] = Str->string_c[i];
    return string_c;
}
#endif /* __cplusplus */


#define _CNS_ERROR_ALLOC_WCSTRING(__FUNCTION)\
    fprintf(stderr, "\n *** libCNString at " #__FUNCTION \
    ", line %d of \"%s\", says:"\
        " failed to create a wchar array!\n", __LINE__, __FILE__)

wchar_t *cns_towcs(const void *_string) {
    size_t size = cns_count(_string)+1;
    if (size == 1 || size > _cns_max_size_set(0)) {
#if !defined(CNS_SUPPRESS_WARNINGS) && !defined(NDEBUG)
        _CNS_WARNING(_towcs());
#endif
        return NULL;
    }
    const char* str = (cns_is(_string) ?
                        CNSTRING(_string)->string_c
                        : CSTRING(_string));
    wchar_t *wstr = cn_new(wchar_t, size);
    if (!wstr) {
        _CNS_ERROR_ALLOC_WCSTRING(_towcs());
        return wstr;
    }
    size_t length = mbstowcs(wstr, str, size);
    if (length != size-1) {
        cn_free(wstr);
        wstr = NULL;
        _CNS_INVALID_LOCALE_SET_WARNING(_towcs());
    }
    return wstr;
}
