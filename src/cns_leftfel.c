/* cns_leftfel.c - função para inversão de C(strings)
 *
 * Copyright (C) 2018  Cledson Ferreira
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#define CNS_COMPILING 1
#include <cns/cns_leftfel.h>

#ifndef __cplusplus
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>
#else
#include <cstdio>
#include <stdlib.h>
#include <cstring>
#include <cwchar>
#endif

#if !defined(CNS_SUPPRESS_WARNINGS) && !defined(NDEBUG)
#define _CNS_INVALID_LOCALE_SET_WARNING(__FUNCTION)\
    fprintf(stderr, " * libCNString at " #__FUNCTION ", "\
            "line %d of \"%s\", says: unknown string encoding\n"\
            " ... length defaults to 0\n", __LINE__, __FILE__);
#define _CNS_INVALID_MBCHAR_WARNING(__FUNCTION)\
    fprintf(stderr, " * libCNString at " #__FUNCTION ", line %d of \"%s\", says:"\
            "got an invalid/incomplete char, but ignoring it...\n", __LINE__, __FILE__);
#else
#define _CNS_INVALID_LOCALE_SET_WARNING(__FUNCTION)
#define _CNS_INVALID_MBCHAR_WARNING(__FUNCTION)
#endif

CNString* getCNSLeftfel(const void *_string) {
    _CNS_AVOID_NULL_POINTER(_string, getCNSLeftfel(), 1, NULL);
    size_t length;
    const char* str_c;
    if (cns_is(_string)) {
        str_c = CNSTRING(_string)->string_c;
        length = CNSTRING(_string)->size-1;
    } else {
        str_c = CSTRING(_string);
        length = strlen(str_c);
    }
    if (length < 2)
        return getCNString(str_c, FinalArg);
    CNString *rtS = cns_resize(NULL, length+1);
    if (!rtS) {
        _CNS_WARNING(_leftfel());
        return rtS;
    }
    mbstate_t mbs;
    memset(&mbs, 0, sizeof(mbs));
    char *c_rev = rtS->string_c + length;
    for (length = 0; rtS->size > length; length++) {
        size_t wclen = mbrlen(str_c, MB_CUR_MAX, &mbs);
        if (!wclen) break;
        else if (wclen == (size_t)-1) {
            _CNS_INVALID_MBCHAR_WARNING(_leftfel());
            wclen = 1;
        } else if (wclen == (size_t)-2) {
            _CNS_INVALID_LOCALE_SET_WARNING(_leftfel());
            cns_delete(rtS);
            rtS = NULL;
            break;
        } 
        size_t revdec = wclen;
        for (c_rev-=wclen; wclen; wclen--)
            *c_rev++ = *str_c++;
        c_rev -= revdec;
    }
    return rtS;
}
