/* cns_arrays.c - funções para trabalho com arrays de cnstrings
 *
 * Copyright (C) 2018  Cledson Ferreira
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#define CNS_COMPILING 1
/* cns_extras.h provoca a inclusão de todos os cabeçalhos */
#include <cns_extras.h>

#ifndef __cplusplus
#include <string.h>
#else
#include <cstring>
#endif

CNString **cns_split(const void *_string, const void* str_seq) {
    _CNS_AVOID_NULL_POINTER(_string, _split(), 1, NULL);
    _CNS_AVOID_NULL_POINTER(str_seq, _split(), 2, NULL);

    size_t size = (cns_is(_string) ? CNSTRING(_string)->size : strlen(CSTRING(_string))+1);
    long array_size = cns_search(_string, str_seq, -1)+2;
    CNString **strings = cn_new(CNString*, (size_t)array_size);
    CNString **re_strings;
    if (!strings) return NULL;
    else if (array_size == 2 || (size_t)(array_size-2) == size) {
        /* String_search() retorna o tamanho da String caso _exp == "" */
        if (!(re_strings = cn_resize(strings, CNString*, 2))) {
            cn_free(strings);
            return re_strings;
        } strings = re_strings;
        strings[0] = getCNString(_string, FinalArg);
        strings[1] = NULL;
        return strings;
    }

    size_t str_size;
    const char *str_c;
    if (cns_is(str_seq)) {
        str_c = CNSTRING(str_seq)->string_c;
        str_size = CNSTRING(str_seq)->size;
    } else {
        str_c = CSTRING(str_seq);
        str_size = strlen(str_c)+1;
    }

    long i, prev_end = 0;
    for (i=0; i < array_size-1; i++) {
        long end = cns_search(_string, str_seq, i);
        if (end == -1)
            end = (long)size-1;
        strings[i] = getCNSCut(_string, prev_end, end);
        prev_end = end + (long)str_size-1;
    }
    strings[i] = NULL;

    /* Exclui Strings vazias */
    i=0;
    while (i < array_size-1) {
        if (strings[i]->size == 1) {
            cns_delete(strings[i]);
            long j;
            for (j=i; j < array_size-1; j++)
                strings[j] = strings[j+1];
            /* Isto é provavelmente desnecessário */
            if (!(re_strings = cn_resize(strings, CNString*,
                                ((size_t)--array_size))))
                return strings;
            strings = re_strings;
            continue;
        }
        i++;
    }
    return strings;
}

#define _CNS_ARRAY_INVALID_ELEMENT(__FUNCTION, __INDEX)\
    fprintf(stderr, "\n *** libCNString at " #__FUNCTION\
        ", line %d of %s says: array's element %lu is not a real String!\n",\
        __LINE__, __FILE__, (unsigned long)__INDEX)

CNString* cns_join(CNString *list[], const void *str_seq) {
    _CNS_AVOID_NULL_POINTER(list, _join(), 1, NULL);

    CNString* Str = getCNString(FinalArg);
    size_t i;
    for (i=0; list[i]; i++) {
        if (!cns_is(list[i])) {
            _CNS_ARRAY_INVALID_ELEMENT(_join, i);
            break;
        }
        cns_append(Str, list[i]);
        if (list[i+1] && str_seq) cns_putstr(Str, Str->size-1, str_seq);
    }
    return Str;
}

bool cns_deleteall(CNString *list[]) {
    _CNS_AVOID_NULL_POINTER(list, delete_Strings(), 1, false);

    size_t i;
    for (i=0; list[i]; i++) {
        /* Caso alguém esqueça do NULL */
        if (!cns_is(list[i])) {
            _CNS_ARRAY_INVALID_ELEMENT(delete_Strings(), i);
            return false;
        }
        cns_delete(list[i]);
        list[i] = NULL;
    }
    return true;
}
