/* cnstring.h - biblioteca para cnstrings em C/C++
 *
 * Copyright (C) 2018  Cledson Ferreira
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

/* Alguns conceitos importantes para entender a documentação:
 * ... string = uma sequência de caracteres delimitada com o caractere nulo.
 * ... C-string, ou string_c = vetor de caracteres da linguagem C com uma string.
 * ... String, ou cnstring = ponteiro para uma CNString.
 * ... C(string), ou _string = uma C-string ou uma cnstring, é um tipo ambíguo
 * ... e possivelmente problemático se o programador não leu esta documentação.
 * ... CNS = biblioteca para manipulações fáceis de cnstring em C/C++.
 */

#ifndef _CNSTRING_H
#define _CNSTRING_H 1

#ifndef __cplusplus
#include <stdbool.h>
#include <stddef.h>
#else
#include <cstddef>
#endif

/* Basicamente: número de lançamento, subversão e data de construção.
 * A CNString poderá não ser mais atualizada após esta versão. */
#define CNS_VERSION_RELEASE 0
#define CNS_VERSION_SUB     11
#define CNS_VERSION_BUILD   510
#define CNS_VERSION_STR     "0.11.510"

#ifndef CNS_COMPILING
#define cn_const const
#else
#define cn_const
#endif
/* Entenda "CN" como uma pequena biblioteca escondida dentro da CNS */

typedef struct _CNString {
    cn_const long unsigned int         __cns_meta;

    /* 'life' é uma alternativa ao abandonado 'to_me', que deve funcionar com a
     * função cns_life_get(), e cns_life_lose() ou cns_autofree(), possibilitando
     * a limpeza de memória da CNString quando as suas vidas acabarem.
     *
     * A rotina 'life' é inspirada no mecanismo de coleção de lixo da biblioteca
     * Glib. */
    cn_const unsigned long             __life;
#ifndef CNS_DISABLE_FEATURES
    struct _CNString* cn_const         __prev;
    struct _CNString* cn_const         __next;
#endif
    cn_const size_t         size;
    cn_const char* cn_const string_c;
} CNString;
typedef CNString*        cnstring;
/* OBS.: exceto os ponteiros 'prev' e 'next', que são bloqueados indiretamente,
 * as demais variáveis da CNString não são protegidas contra "condição de
 * corrida" (data race). Por enquanto, você deve se certificar que as tarefas do
 * seu programa não modificarão as variáveis simultaneamente. */

/* Tratar o ponteiro como uma cnstring, uma C-string ou uma C(string) */
#define CNSTRING(POINTER)   ((cnstring)POINTER)
#define CSTRING(POINTER)    ((char*)POINTER)

/* cn_new aloca memória para um array de TYPE */
#define cn_new(TYPE, SIZE)      (TYPE*)malloc((SIZE)*sizeof(TYPE))
/* cn_znew() aloca memória para um array de TYPE zerado */
#define cn_znew(TYPE, SIZE)     (TYPE*)calloc(sizeof(TYPE), SIZE)
#define cn_resize(POINTER, TYPE, SIZE)\
    (TYPE*)realloc((POINTER), sizeof(TYPE)*(SIZE))
#define cn_free(POINTER)        free(POINTER)

#ifdef _WIN32
#define CN_ON_WINDOWS 1
#define cn_sleep(SECS) Sleep((SECS)*1000)
/* Defina CN_USE_PTHREAD se quiser usar a biblioteca pthread no Windows */

#elif defined(__unix__) || defined(__unix)
#define CN_ON_UNIX 1
#define cn_sleep(SECS) sleep(SECS)

/* Alterado de CNS_DONT_USE para CNS_DISABLE para manter a convenção */
#ifndef CNS_DISABLE_PTHREAD
/* Isto não deve surtir efeito se CNS_DISABLE_FEATURES foi definido! */
#define CN_USE_PTHREAD 1
#endif
#else
#error *** LIBCNSTRING: NOT SUPPORTED PLATFORM!
#endif /* _WIN32 */

#include "cnmutex.h"


#ifndef CNS_DISABLE_FEATURES
/* As macros seguintes são interfaces de funções padrões para evitar
 * vazamento de memória ao abortar o programa.
 * Basicamente, antes de abortar a execução, cns_kill() será chamado. */
#ifdef CN_ON_UNIX
#define cns_assert(EXPR) if (!(EXPR))\
    _cns_assert(#EXPR, __LINE__, __func__, __FILE__)
#else
#define cns_assert(EXPR) if (!(EXPR))\
    _cns_assert(#EXPR, __LINE__, __FUNCTION__, __FILE__)
#endif /* __unix && !NDEBUG*/

/* Isto não é adequado para uso dentro da biblioteca, mas fora dela */
#define cns_abort() cns_kill(); abort()

#else
#define cns_assert(EXPR)
#define cns_abort()
#endif /* !CNS_DISABLE_FEATURES */


/* Macros para avisos (d)e erros */
#ifdef CNS_COMPILING
/* Os avisos logo abaixo requerem isso */
#if defined(CNS_SUPPRESS_WARNINGS) || defined(_CNS_EXTRAS_H)
/* não incluimos nada */
#elif defined(__cplusplus)
#include <cstdio>
#else
#include <stdio.h>
#endif

#define _CNS_DEFAULT_MAX_SIZE 524288

/* Este aviso é sempre acompanhado de um erro, então não deve ser como os demais */
#define _CNS_WARNING(__FUNCTION)\
    fprintf(stderr, " ... at " #__FUNCTION ", line %d of \"%s\"\n", __LINE__,\
            __FILE__)

#if !defined(CNS_SUPPRESS_WARNINGS) && !defined(NDEBUG)
#define _CNS_MAXSIZE_OVERFLOW_WARNING(__FUNCTION)\
    fprintf(stderr, "\n * libCNString at " #__FUNCTION\
        ", line %d of \"%s\", says: string is too long, I need to stop!\n",\
        __LINE__, __FILE__)

#define _CNS_NULL_POINTER_WARNING(__FUNCTION, __ARG_N)\
    fprintf(stderr, "\n * libCNString at " #__FUNCTION \
        ", line %d of \"%s\", says:"\
        " got a NULL pointer as argument %d, returning...\n",\
        __LINE__, __FILE__, __ARG_N)

#define _CNS_NULLARG_WARNING()\
    printf(" ... pass NULL as a final arg of a variadic fun is deprecated"\
        " because it may leads to an undefined behavior.\n"\
        "Use the FinalArg macro instead. It's different and cool\n")

#else
#define _CNS_MAXSIZE_OVERFLOW_WARNING(__FUNCTION)
#define _CNS_NULL_POINTER_WARNING(__FUNCTION, __ARG_N)
#define _CNS_NULLARG_WARNING()
#endif /* !CNS_SUPPRESS_WARNINGS && !NDEBUG */

#define _CNS_AVOID_NULL_POINTER(__ARG_V, __FUNCTION, __ARG_N, __RET_V)\
    if (!(__ARG_V)) {\
        _CNS_NULL_POINTER_WARNING(__FUNCTION, __ARG_N);\
        return __RET_V; \
    }

#define _CNS_ERROR_INVALID_INDEX(__FUNCTION, __INDEX, __SIZE)\
    fprintf(stderr, "\n *** libCNString at " #__FUNCTION \
            ", line %d of \"%s\" says:"\
            " index out of bounds: %lu >= %lu\n", __LINE__, __FILE__,\
            (unsigned long) __INDEX, (unsigned long) __SIZE);
#endif

/** Configura o comprimento máximo (incluindo caractere nulo) das cnstrings.
 * O tamanho máximo padrão é 524288, ou 512 KBytes.
 *
 * O tamanho máximo pode variar de 1 até 2^(8*sizeof int)-1.
 *
 * @param max_size - o tamanho máximo das cnstrings (deve ser um número > 1).
 * @return __max_size.
 */
size_t _cns_max_size_set(size_t max_size);


/** Verifica se a versão da biblioteca instalada é menor ou equivalente
 * à biblioteca do programa compilado.
 * 
 * @param ver_release - número de lançamento (X.x.xxxx)
 * @param ver_sub - subversão (x.X.xxxx)
 * @param ver_build - data de construção (x.x.XXXX)
 */
bool cns_version_check(int ver_release, int ver_sub, int ver_build);


/** Retorna uma C-string representando a versão da biblioteca instalada.
 * 
 * @return C-string.
 */
const char *cns_version_str_get();

/** Testa se a C(string) 'maybeCNString' é uma cnstring (i.e., aponta para uma
 * CNString).
 *
 * Para que isso seja possível, é calculado os metadados de 4 chars gerados pela
 * primeira cnstring.
 *
 * Os metadados ficam na cabeça das cnstrings, então, caso queira ler os metadados
 * em seu programa, atribua a cnstring num ponteiro para char e acesse as primeiras
 * quatro posições do vetor. Para mais detalhes, veja "cnstring.c".
 *
 * @param maybeCNString - C(string).
 * @return false - não é uma cnstring.
 */
bool cns_is(const void *maybeCNString);

#ifndef CNS_DISABLE_FEATURES

void _cns_init() __attribute__((constructor));

/** Libera a memória alocada para as cnstrings.
 * Está marcada com (destructor) para ser chamada no encerramento do programa
 * ...mas isso só funciona com o Clang ou o GCC. Se usar outro compilador, chame
 * cns_init() no início de main().
 */
void cns_kill() __attribute__((destructor));

void _cns_assert(const char *expr,
                 int src_line,
                 const char *src_func,
                 const char *src_fname) __attribute__((noreturn));
#endif

/** Esta macro só pode/deve ser utilizada em _print(), _printerr(),
 * getCNString() e _cns_update()
 * Errata: FinalArg agora é 0 para que seja possível usar uma função que retorne
 * uma cnstring OU um ponteiro nulo.
 */
#define FinalArg ((void*)0)

/** Imprime C(strings).
 * O último parâmetro deve ser FinalArg.
 *
 * Se qualquer parâmetro for diferente de uma C(string), bugs vão acontecer. Use
 * printf() para imprimir strings com múltiplos tipos de variáveis.
 *
 * @param _string - C(string) */
void _cns_print(const void *_string, ...);

/** O mesmo que _cns_print(), mas imprime na saída de erro
 *
 * @param _string - C(string) */
void _cns_printerr(const void *_string, ...);


/** Cria uma cnstring vazia com uma vida.
 *
 * As CNString alocadas de forma dinâmica são desde já temporárias. Chame
 * _setto_me(nome, &nome) para mudar a referência da variável que
 * mantém a cnstring.
 * 
 * Todas as funções que criam ou clonam CNStrings chamam esta para alocar memória
 * para a nova CNString.
 *
 * @return cnstring. */
cnstring newCNString();

/** Converte uma ou mais C(strings) numa cnstrings concatenada e retorna.
 * O último parâmetro deve ser FinalArg.
 *
 * Se qualquer parâmetro for diferente de uma C(string), bugs vão acontecer. Use
 * cns_format() para construir strings com múltiplos tipos de variáveis.
 *
 * @param _string - C(string), FinalArg para criar uma cnstring vazia.
 * @return cnstring, NULL - erro. */
cnstring getCNString(const void *_string, ...);


#ifdef _CNS_EXTRAS_H
#include "cns/cns_arrays.h"
#include "cns/cns_cut.h"
#include "cns/cns_leftfel.h"
#include "cns/cns_search.h"
#include "cns/cns_string.h"
#endif


/** Altera o conteúdo de uma cnstring existente.
 *
 * Caso '_string' seja FinalArg, apenas redimensiona a string para combinar
 * com o comprimento do conteúdo.
 * Pelo padrão do CNS, é desnecessário chamar isto após manipular cnstrings com as
 * funções da própria biblioteca.
 *
 * Esta função segue as mesmas regras de parâmetros da função getCNString().
 *
 * @param Str - String.
 * @param _cns_string - uma C-string ou C(string).
 * @return String, NULL - erro.
 */
cnstring _cns_update(cnstring Str, const void *_cns_string, ...)
  __attribute__((nonnull (1)));


/** Atualiza a cnstring com uma string formatada.
 *
 * Observações:
 * Se essa biblioteca foi compilada com o GCC/Clang, cns_format() usa a
 * função vasprintf() do GNU para construir strings formatadas, caso contrário,
 * vsnprintf() é quem será chamada.
 *
 * @param Str - cnstring.
 * @param format - uma C-string de formato.
 * @return String, NULL - erro.
 */
cnstring cns_format(cnstring Str, const char *format, ...)
    __attribute__((format (printf, 2, 3)));


/** Altera o tamanho da String, ou cria uma String "aleatória" com determinado
 * tamanho.
 *
 * Se 'Str' == NULL, cria cnstring com tamanho <size>.
 * Se <size> == 1 -> a String fica vazia, com <size> = 1.
 * Se <size> == 0 -> a String é deletada da memória, como realloc(void*, 0).
 *
 * @param Str - String a ser modificada
 * @param size - tamanho em bytes (strlen+1)
 */
cnstring cns_resize(cnstring Str, size_t size);

#ifndef CNS_DISABLE_FEATURES
/** Substitui o núcleo da cnstring com segurança.
 * 'str' PRECISA ter sido alocada com malloc/realloc e ter um delimitador '\0',
 * como toda string do C.
 *
 * @param Str - String para colocar 'str'.
 * @param str - C-string alocada dinamicamente na memória.
 * @return String, NULL - erro.
 */
cnstring _cns_remount(cnstring Str, char* str) __attribute__((nonnull));
#endif

/** Copia conteúdo de 'src' para 'dest'.
 *
 * @param dest - String a receber os dados de 'src'.
 * @param src - String do qual os dados serão copiados.
 * @return String, NULL - erro.
 */
cnstring cns_copy(cnstring dest, cnstring src) __attribute__((nonnull));


/** Adiciona conteúdo de 'src' ao final de 'dest'.
 *
 * @param dest - String a receber os dados de 'src'.
 * @param src - String do qual os dados serão copiados.
 * @return String, NULL - erro.
 */
cnstring cns_append(cnstring dest, cnstring src) __attribute__((nonnull));


/** Cria uma cópia da String.
 * Se nulo, então retorna uma String vazia.
 *
 * @param Str String para ser duplicada.
 * @return String - clone, NULL - erro.
 */
cnstring cns_clone(cnstring Str) __attribute__((nonnull));


/** Compara se duas C(strings) são iguais.
 *
 * @return true - se as C(strings) são iguais, false - diferentes
 * ou um argumento é nulo.
 */
bool cns_equals(const void *str1, const void *str2);


/** Retorna a *capacidade* da cnstring, ou seja, o tamanho da memória alocada para
 * o vetor.
 *
 * @param Str - cnstring.
 * @return tamanho da cnstring em bytes.
 */
static inline size_t cns_size(cnstring Str) {
    return Str->size;
}


/** Esta função retorna a string de Str para uso com funções padrões C.
 * Para manipulações manuais, utilize cns_size(Str) como limite para não
 * ocorrer estouro de buffer. Lembrar sempre que '\0' deve estar na última
 * posição possível do vetor.
 * Após terminar qualquer operação de escrita na string, cns_update(Str, FinalArg)
 * deve ser chamada, pois é possível que o tamanho da string seja alterado com um
 * '\0'.
 *
 * @param Str - cnstring.
 * @return const char*.
 */
static inline const char *cns_get(cnstring Str) {
    return Str->string_c;
}


/** Retorna o caractere em <index>
 * Caso o ponteiro seja nulo ou a cnstring seja vazia, retorna o caractere nulo.
 *
 * @param Str - cnstring.
 * @param index - posição do caractere.
 * @return char.
 */
static inline char cns_getchar(cnstring Str, size_t index) {
    return (index < Str->size-1 ? Str->string_c[index] : '\0');
}


/** Altera o caractere de 'Str' em <index> para 'c'.
 * A alteração só será efetiva se <index> < tamanho-1 de 'Str'.
 * Se c == 0, corta a cnstring.
 *
 * @param Str - cnstring.
 * @param index - índice para substituir.
 * @param c - caractere substituto.
 * @return cnstring.
 */
cnstring cns_setchar(cnstring Str, size_t index, char c)
    __attribute__((nonnull (1)));


/** Adiciona 'c' na posição <index> de 'Str'.
 * A alteração só será efetiva se <index> < tamanho de 'Str'.
 * Se c == 0, corta a cnstring.
 *
 * @param Str - cnstring.
 * @param index - índice para colocar o caractere.
 * @param c - caractere adicional.
 * @return cnstring.
 */
cnstring cns_putchar(cnstring Str, size_t index, char c)
    __attribute__((nonnull (1)));

static inline cnstring cns_addchar(cnstring Str, char c) {
    return cns_putchar(Str, Str->size-1, c);
}


/** Remove um caractere em <index> de 'Str'.
 *
 * @param Str - cnstring.
 * @param index - índice para remover o caractere.
 * @return cnstring.
 */
cnstring cns_remchar(cnstring Str, size_t index)
    __attribute__((nonnull (1)));


/** Desaloca os blocos de memória da (remove a) cnstring.
 * 
 * Em sistemas multitarefas, é possível, mas bastante improvável, que o uso
 * intensivo de cnstrings cause alguma falha ao removê-la.
 *
 * @param Str - cnstring.
 * @return true, false - erro. */
bool cns_delete(cnstring Str) __attribute__((nonnull));


/** Adiciona uma vida à CNString.
 * cns_life_get() não faz qualquer verificação na cnstring. Não passe ponteiros
 * nulos sob qualquer hipótese!
 * 
 * @param Str.
 * @return Str. */
cnstring cns_life_get(cnstring Str) __attribute__((nonnull));

/** Retira uma vida da CNString.
 * Esta função pode remover a CNString (com _delete()) se sua vida chegou a 0.
 * 
 * @param Str.
 * @return cnstring, NULL - sem vida. */
cnstring cns_life_lose(cnstring Str) __attribute__((nonnull));


/** Caso você seja paranóico e queira uma remoção perfeita ;)
 * Isto zera toda a cnstring antes de apagar, inclusive a variável associada a ela.
 *
 * @param StrPass - cnstring contendo informação ultraconfidencial.
 * @return true, false - erro. */
bool cns_zdelete(cnstring StrPass) __attribute__((nonnull));

#ifdef __GNUC__
#define GCC_VERSION (__GNUC__*10000 + __GNUC_MINOR__*100 + __GNUC_PATCHLEVEL__)
#else
#define GCC_VERSION 0
#endif

#ifndef CNS_COMPILING
#if GCC_VERSION < 30101
#ifndef CNS_DISABLE_FEATURES
/* Se não compilar com Clang e nem com o GCC, chame cns_init() no início da
 * função main() */
#define cns_init() {\
    _cns_init();\
    atexit(cns_kill);\
}
#else
#define cns_init()
#endif

#ifdef CNS_DISABLE_FEATURES
#define cn_life(Type) {\
        _cns_printerr("*** libCNString says: cn_life(" #Type ") is not"\
                      " supported on this compiler!\n", FinalArg);\
        exit(1);\
    } Type
#else
#define cn_life(Type) Type
#endif

#define cn_life_list(ListType) {\
        _cns_printerr("*** libCNString says: cn_life(" #ListType ") is not"\
                      " supported on this compiler!\n", FinalArg);\
        exit(1);\
    } ListType

#else
#define cns_init() /* a mágica do nada */

/* cn_life() é baseado na macro g_autoptr() da Glib, e somente funciona com o GCC
 * e com o Clang.
 * 
 * Se você criar um tipo de objeto NyanCat e quiser que essa variável seja
 * descartada ao encerrar um bloco de código, defina uma função com o nome:
 * | static inline void NyanCat_autolose(NyanCat **nyan) {
 * |    if (*nyan) nyan_life_lose(*nyan);
 * | }
 * 
 * Caso você não queira configurar um cn_life, troque _lose() pela nyan_delete()
 * de sua biblioteca/programa. */
#define cn_life(Type)\
 __attribute__((__cleanup__(Type##_life_autolose))) Type

/* Se o array foi alocado no heap */
#define cn_life_list(ListType)\
 __attribute__((__cleanup__(ListType##_life_list_autolose))) ListType


static inline void cnstring_life_autolose(cnstring *cnstr) {
    if (*cnstr) cns_life_lose(*cnstr);
}

static inline void cnstring_life_list_autolose(cnstring **list) {
    cnstring *mlist = *list;
    while (*mlist) cns_life_lose(*(mlist++));
    free(*list);
}
#endif /* GCC_VERSION */


#define cns_new                    newCNString
#define cnstring(...)              getCNString(__VA_ARGS__, FinalArg)

#define cns_update(__CNSTRING, ...)\
    _cns_update((__CNSTRING), __VA_ARGS__, FinalArg)

/* Macros para leitura de informações
 * cns_str() converte C(string) em C-string. */
#define cns_str(__STRING)       (!__STRING ? "(nil)" :\
    (cns_is(__STRING) ?\
        cns_get(CNSTRING(__STRING)) :\
        CSTRING(__STRING)))
#define cns_isempty(__STRING)\
    (cns_is(__STRING) ? cns_size(CNSTRING(__STRING))==1 :\
        *(CSTRING(__STRING))==0)

#define cns_len(__CNSTRING)     (cns_size(__CNSTRING)-1)

#define cns_println(...)        _cns_print(__VA_ARGS__, "\n", FinalArg)
#define cns_print(...)          _cns_print(__VA_ARGS__, FinalArg)
#define cns_printerr(...)       _cns_printerr(__VA_ARGS__, FinalArg)

/* Retornam novas cnstrings */
#define cns_leftfel              getCNSLeftfel
#define cns_cut                  getCNSCut
#define cns_UPPER(__STRING)      cns_setUPPER(getCNString(__STRING, FinalArg))
#define cns_lower(__STRING)      cns_setlower(getCNString(__STRING, FinalArg))

/* Verifica se uma C(string) está contida na cnstring */
#define cns_contains(__STRING_A, __STRING_X)\
        (cns_search((__STRING_A), (__STRING_X), 0) >= 0)

#endif /* !CNS_COMPILING */
#endif /* _CNSTRING_H */


/* Eu fiz tudo isso sozinho até agora, mas aprendi muita coisa com códigos de
 * outros programadores experientes, incluindo o código da biblioteca "stdio.h",
 * a biblioteca "stdlib.h", a "iostream", e através de sites online como:
 * * Uma referência das bibliotecas padrões das linguagens C e C++
 *   [http://cplusplus.com/reference];
 * * Uma apostila de C/C++ gratuita
 *   [http://www.cprogressivo.net/];
 * * Discussões em fórums sobre funções com parâmetros variáveis
 *
 * by cleds'upper */
