/* cnmutex.h - interfaces para as variáveis de exclusão mútua do POSIX e do Windows. */
#ifndef _CNMUTEX_H
#define _CNMUTEX_H 1

#ifdef CN_USE_PTHREAD
#include <pthread.h>
typedef pthread_mutex_t cnmutex;

static inline bool cnmutex_init(cnmutex *mutex) {
    return !pthread_mutex_init(mutex, NULL);
}

static inline bool cnmutex_destroy(cnmutex *mutex) {
    return !pthread_mutex_destroy(mutex);
}

static inline bool cnmutex_timedlock(cnmutex *mutex, time_t secs) {
    struct timespec wait_ts;
    clock_gettime(CLOCK_REALTIME, &wait_ts);
    wait_ts.tv_sec += secs;
    return !pthread_mutex_timedlock(mutex, &wait_ts);
}

static inline bool cnmutex_lock(cnmutex *mutex) {
    return !pthread_mutex_lock(mutex);
}

static inline bool cnmutex_trylock(cnmutex *mutex) {
    return !pthread_mutex_trylock(mutex);
}

static inline bool cnmutex_unlock(cnmutex *mutex) {
    return !pthread_mutex_unlock(mutex);
}

#elif defined(CN_ON_WINDOWS)
#ifndef __cplusplus
#include <time.h>
#else
#include <ctime>
#endif

#include <windows.h>
typedef HANDLE cnmutex;

static inline bool cnmutex_init(cnmutex *mutex) {
    return (*mutex = CreateMutex(NULL, FALSE, NULL)) != NULL;
}

static inline bool cnmutex_destroy(cnmutex *mutex) {
    return CloseHandle(*mutex) != 0;
}

static inline bool cnmutex_timedlock(cnmutex *mutex, time_t secs) {
    DWORD result = WaitForSingleObject(*mutex, secs*1000);
    return (result == WAIT_OBJECT_0 || result == WAIT_ABANDONED);
}

static inline bool cnmutex_lock(cnmutex *mutex) {
    DWORD result = WaitForSingleObject(*mutex, INFINITE);
    return (result == WAIT_OBJECT_0 || result == WAIT_ABANDONED);
}

static inline bool cnmutex_trylock(cnmutex *mutex) {
    DWORD result = WaitForSingleObject(*mutex, 0);
    return (result == WAIT_OBJECT_0 || result == WAIT_ABANDONED);
}

static inline bool cnmutex_unlock(cnmutex *mutex) {
    return ReleaseMutex(*mutex) != 0;
}
#endif
#endif
