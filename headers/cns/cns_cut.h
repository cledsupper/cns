/* cns_cut.h - função para cortar strings
 *
 * Copyright (C) 2018  Cledson Ferreira
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef _CNS_CUT_H
#define _CNS_CUT_H 1
#include <cnstring.h>

/* Faz uma cópia recortada da cnstring.
 * Corta do final ao começo se <start> > <end>. Baseado em _leftfel().
 * O limite do corte, geralmente <end>, deve ser menor que o tamanho da C(string)
 * 'cut'.
 *
 * @param _string - C(string)
 * @param start - índice inicial do corte, ou final se for invertido.
 * @param end - final do corte, ou início se for invertido.
 * @return cnstring, NULL - erro.
 */
cnstring getCNSCut(const void *_string, long start, long end)
    __attribute__((nonnull));
#endif