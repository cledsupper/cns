/* cns_string.h - funções para usos gerais de (C/cn)(w)strings
 *
 * Copyright (C) 2018  Cledson Ferreira
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef _CNS_STRING_H
#define _CNS_STRING_H 1

/** Retorna o número de caracteres multibyte da C(string).
 * 
 * @param _string - C(string).
 * @return size_t - quantidade de caracteres da C(string).
 */
size_t cns_count(const void *_string) __attribute__((nonnull));

/** Substitui os caracteres de 'Str' a partir de <index> para os caracteres da
 * 'string_c'.
 * Baseada em String_setchar().
 * Diferente de String_setchar(), aumenta o tamanho da String quando for
 * necessário.
 *
 * @param Str - String que receberá os caracteres.
 * @param index - posição onde o primeiro caractere será substituído.
 * @param _string - como o nome sugere, é a string substituta, podendo ser
 * uma C-string ou String.
 * @return String, NULL - erro.
 */
cnstring cns_setstr(cnstring Str, size_t index, const void *_string)
    __attribute__((nonnull (1, 3)));


/** Insere os caracteres de 'string_c' em 'Str' a partir de <index>.
 * Baseada em String_putchar().
 * Concatena-se com:
 * String_putstr(Str, cnstring_size(Str)-1, string_c)
 *
 * @param Str - String que receberá os caracteres.
 * @param index - posição onde o primeiro caractere será inserido.
 * @param _string - C-string ou CNString.
 * @return String, NULL - erro.
 */
cnstring cns_putstr(cnstring Str, size_t index, const void *_string)
    __attribute__((nonnull (1, 3)));

static inline cnstring cns_addstr(cnstring Str, const void *_string) {
  return cns_putstr(Str, Str->size-1, _string);
}


/** Remove uma sequência de 'Str' igual a 'string_c' a partir da <skip+1>ª
 * ocorrência.
 *
 * @param Str - String operada.
 * @param _string - C(string) com um trecho de Str.
 * @param skip - número da ocorrência que será removida.
 * @return String, NULL - erro.
 */
cnstring cns_remstr(cnstring Str, const void *_string, unsigned int skip)
    __attribute__((nonnull (1, 2)));


/** Altera todos os caracteres minúsculos para MAIÚSCULOS.
 *
 * @param _string - C(string).
 * @return C(string).
 */
void* cns_setUPPER(void *_string) __attribute__((nonnull));


/** Altera todos os caracteres MAIÚSCULOS para minúsculos.
 *
 * @param _string - C(string).
 * @return C(string).
 */
void* cns_setlower(void *_string) __attribute__((nonnull));


/** Converte uma C(string) multibyte em uma wide string malloc'ada.
 *
 * @param _string - C(string).
 * @return wide string, NULL - erro.
 * */
wchar_t *cns_towcs(const void *_string) __attribute__((nonnull));


#ifdef __cplusplus
#include <string>
std::string cns_tostring(std::string& string_c, cnstring Str);

char* cns_tostringc(char *string_c, cnstring Str);
#else
/* é necessário que 'string_c' suporte 'Str' */
char* cns_tostring(char *string_c, cnstring Str);
# define cns_tostringc(__CSTRING, __CNSTRING)\
    cns_tostring((__CSTRING), (__CNSTRING))
#endif /* __cplusplus */

#endif
