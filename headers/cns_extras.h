/* cns_extras.h - manipulação da CNString com outros tipos de objetos.
 *
 * Copyright (C) 2018  Cledson Ferreira
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef _CNS_EXTRAS_H
#define _CNS_EXTRAS_H 1

#ifdef __cplusplus
#include <cstdio>
#include <cstdlib>
#include <cwchar>
#else
#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>
#endif /* __cplusplus */

#ifdef _CNSTRING_H
#error YOU MUST INCLUDE ME (cns_extras.h) INSTEAD OF HIM (cnstring.h)!!! :(
#else
#include "cnstring.h"
#endif

/** Semelhante a fgets(), mas não adiciona '\n' ao final da cnstring.
 *
 * @param Str - cnstring.
 * @param _instream - arquivo para leitura.
 * @return cnstring, NULL - erro.
 */
cnstring cns_freadline(cnstring Str, FILE *_instream) __attribute__((nonnull));

#define cns_input(__CNSTRING)   cns_freadline((__CNSTRING), stdin)

/** Lê todo o fluxo '_instream' em 'Str'.
 *
 * @param Str - cnstring.
 * @param _instream - arquivo para leitura.
 * @return cnstring, NULL - erro.
 */
cnstring cns_fread(cnstring Str, FILE *_instream) __attribute__((nonnull));

/** Escreve 'Str' em '_outstream'.
 *
 * @param Str - cnstring.
 * @param _outstream - arquivo para escrita.
 * @return _outstream, NULL - erro.
 */
FILE *cns_fwrite(cnstring Str, FILE *_outstream) __attribute__((nonnull));

/* Todas as funções a seguir convertem as Strings para tipos numericos.
 * São baseadas nas funções atoT(), então elas seguem o mesmo comportamento.
 */
static inline int cns_toint(cnstring Str) {
    return atoi(Str->string_c);
}

static inline float cns_tofloat(cnstring Str) {
    return (float)atof(Str->string_c);
}

static inline double cns_todouble(cnstring Str) {
    return atof(Str->string_c);
}

static inline long cns_tolong(cnstring Str) {
    return atol(Str->string_c);
}

static inline short cns_toshort(cnstring Str) {
    return (short) atoi(Str->string_c);
}

#endif /* _CNS_EXTRAS_H */
